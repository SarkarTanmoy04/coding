#figure out whether the numeric is negative or positive or zero
num=int(input('Enter a numeric value: '))
if num<0:
    print('{} is negative number'.format(num))
elif num>0:
    print('{} is positive number'.format(num))
elif num==0:
    print('{} is zero'.format(num))
else:
    print('{} is not numeric value'.format(num))
