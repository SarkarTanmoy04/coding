#max and min value in given list

from random import randrange

size=int(input('Enter the size of list: '))
a=[]
for x in range(1,size+1):
    num=int(input('Enter {} value: '.format(x)))
    a.append(num)

print(a)

#figuring out max
# Technique 1
# using max function
print(max(a))
#technique 2
a.sort()
print(f'{a[-1:]} is the maximum value')

#figuring out min
#technique 1
# using min function
print(min(a))

#technique 2
#already sorted one time, so no need
print(f'{a[0]} is the minimum value')
