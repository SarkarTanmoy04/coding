#figuring out square root
# square root formula is 1/2 or 0.5

# technique 1
num=float(input('Enter a number: '))
num=num**0.5
print(round(num,2))

#technique 2
import math
num2=float(input('Enter a number: '))
value=math.sqrt(num2)
print(value)