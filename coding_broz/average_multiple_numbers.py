# average value of multiple numbers
n=int(input('Enter the number of element: '))
a=[]
for x in range(n):
     value=float(input(f'Enter {x+1} numbered elemenet: '))
     a.append(value)

avg=sum(a)/len(a)
print("The average value is {}".format(round(avg,2)))