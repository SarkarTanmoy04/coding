from audioop import reverse


a='tanmoy'
print(a[:1])
print(a[-1])
print(a[:-1])
print(f'\n{a}')
# double colon using pattern of string [start:stop:step]
print(a[::3])
print(a[::-1])

#technique 2
print('\ntechnique 2')
for x in a[::-1]:
    print(x)