#convert kilometer to mile
#mile to kilometer


#converting kilometer to mile
def kilo_to_mile():
    kilo_meter=float(input('Enter kilometer: '))
    mile=kilo_meter*0.621371
    print(f'{kilo_meter} kilometer = {round(mile,2)} mile')


#converting mile to kilometer
def mile_to_kilo():
    mile=float(input('Enter mile: '))
    kilometer=mile*1.60934
    print(f'{mile} mile = {round(kilometer,2)} kilometers')

#asking for option
print('1 : Kilometer to Mile conversion \n')
print('2 :  Mile to Kilometer conversion')
while(True):
    choice=int(input('\nEnter your choice: '))
    if choice==1:
        kilo_to_mile()
        break
    elif choice==2:
        mile_to_kilo()
        break
    else:
        print('Wrong Choice !!')