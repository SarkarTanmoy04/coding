#print the list of network interfaces available in device
#we have used "psutil package" over here, if its not installed in system
#then install it
import psutil
def interfaces():
    scanner=psutil.net_if_addrs()
    nodes=[]
    for interface_name,_ in scanner.items():
        nodes.append(str(interface_name))
    for _ in range(len(nodes)):
        print(f"{_+1}  {nodes[_]}")



interfaces()