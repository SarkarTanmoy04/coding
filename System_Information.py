import platform
from tabulate import tabulate

thisdata={
    "System": [f"{platform.uname().system}"],
    "Node": [f"{platform.uname().node}"],
    "Release": [f"{platform.uname().release}"],
    "Version": [f"{platform.uname().version}"],
    "Machine": [f"{platform.uname().machine}"],
    "Processor": [f"{platform.uname().processor}"]
}
table=tabulate(thisdata,headers='keys',tablefmt='fancy_grid')
print(table)